
import { parse } from 'csv-parse';

import * as fs from 'fs';

const filePath = './kepler_data.csv';

const habitablePlanets = [];

const parseOptions = {
    //comment symbol used in data file
    comment: "#",
    //return each column as an object instead of array of values
    columns: true
}

/*function that checks if planet record is habitable
    This is done by checking the koi_disposition column value
        to be confirmed, the koi_insolation for stellar flux which measures the amount of light or energy the planet gets
            shouldn't be more than 1.1x Earth gets or it will get hot
            shouldn't be less than 0.36x Earth gets or temperature will get cold
    
    Another factor is checking the size of a planet, where it can't be larger than 1.6x the size of earth
        we use the koi_prad
*/
function isHabitablePlanet(planet) {
    return planet['koi_disposition'] === 'CONFIRMED'
        && planet['koi_insol'] > 0.36
        && planet['koi_insol'] < 1.11
        && planet['koi_prad'] < 1.6;
}

const stream = fs.createReadStream(filePath)
    .pipe(parse(parseOptions)
);

stream.on('data', (data) => {
    const planet = {
        ...data
    }
    
    if (isHabitablePlanet(planet))
    {
        habitablePlanets.push(data);
    }
})
    .on('error', (error) => {
        console.log(`ERROR: ${error}`);
    })
    .on('end', () => {
        console.log(`Found: ${habitablePlanets.length} habitable planets`);

        if (habitablePlanets.length > 0) {
            console.log(habitablePlanets.map(planet => {
                return planet['kepler_name'];
            }));
        }
        console.log('DONE processing file');
    });